/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.semester.registration.model.ui.data;

/**
 *
 * @author student
 */
public class DepartmentDetails {
    private Integer id;
    private String DepartmentName;
    private String DepartmentShortName;

    public DepartmentDetails(Integer id, String DepartmentName, String DepartmentShortName) {
        this.id = id;
        this.DepartmentName = DepartmentName;
        this.DepartmentShortName = DepartmentShortName;
    }
    @Override
    public String toString(){
    
        return DepartmentName+"-"+DepartmentShortName;
    
}
    public Integer getId() {
        return id;
    }

    public String getDepartmentName() {
        return DepartmentName;
    }

    public String getDepartmentShortName() {
        return DepartmentShortName;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setDepartmentName(String DepartmentName) {
        this.DepartmentName = DepartmentName;
    }

    public void setDepartmentShortName(String DepartmentShortName) {
        this.DepartmentShortName = DepartmentShortName;
    }
    
}
