/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.student.semester.registration.service;

import in.ac.gpckasaragod.student.semester.registration.model.ui.data.SemesterRegistrationDetails;
import in.ac.gpckasaragod.student.semester.registration.ui.SemesterRegistrationDetailsForm;
import java.util.List;

/**
 *
 * @author student
 */
public interface SemesterRegistrationDetailsService {
         public String saveSemesterRegistrationDetails(String candidateName, String registerNo, String gender, String departmentId, String currentSem, String revision);
         public SemesterRegistrationDetails readSemesterRegistrationDetails(Integer Id);
         public List<SemesterRegistrationDetails> getAllSemesterRegistrationDetails();
         public String updateSemesterRegistrationDetails(Integer selectedId, String candidateName, String registerNo, String gender, String departmentId, String currentSem, String revision);
         public String deleteSemesterRegistrationDetails(Integer id);
         
}
