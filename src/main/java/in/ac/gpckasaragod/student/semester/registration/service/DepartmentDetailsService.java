/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.student.semester.registration.service;

import in.ac.gpckasaragod.student.semester.registration.model.ui.data.DepartmentDetails;
import in.ac.gpckasaragod.student.semester.registration.ui.DepartmentDetailsForm;
import java.util.List;

/**
 *
 * @author student
 */
public interface DepartmentDetailsService{
    public String saveDepartmentDetails(String Department_Name,String Short_Name);
    public DepartmentDetails readDepartmentDetail(Integer Id);
    public List<DepartmentDetails> getAllDepartmentDetails();
    public String updateDepartmentDetails(Integer id,String departmentName,String shortName);
     public String deleteDepartmentdetails(Integer id);
}
