/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.semester.registration.model.ui.data;

/**
 *
 * @author student
 */
public class SemesterRegistrationDetails {
    private Integer id;
    private String candidateName;
    private String registerNo;
    private String gender;
    private Integer departmentId;
    private String currentSem;
    private Integer revision;

    public SemesterRegistrationDetails(Integer id, String candidateName, String registerNo, String gender, Integer departmentId, String currentSem, Integer revision) {
        this.id = id;
        this.candidateName = candidateName;
        this.registerNo = registerNo;
        this.gender = gender;
        this.departmentId = departmentId;
        this.currentSem = currentSem;
        this.revision = revision;
    }

   

    

    


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public String getCurrentSem() {
        return currentSem;
    }

    public void setCurrentSem(String currentSem) {
        this.currentSem = currentSem;
    }

    public Integer getRevision() {
        return revision;
    }

    public void setRevision(Integer revision) {
        this.revision = revision;
    }
    
}
