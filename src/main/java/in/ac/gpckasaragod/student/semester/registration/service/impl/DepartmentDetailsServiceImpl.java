/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.semester.registration.service.impl;

import in.ac.gpckasaragod.student.semester.registration.model.ui.data.DepartmentDetails;
import in.ac.gpckasaragod.student.semester.registration.service.DepartmentDetailsService;
import in.ac.gpckasaragod.student.semester.registration.ui.DepartmentDetailsForm;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author student
 */
public class DepartmentDetailsServiceImpl extends ConnectionServiceImpl implements DepartmentDetailsService {

    
    @Override
    public String saveDepartmentDetails(String departmentName, String shortName) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = " INSERT INTO DEPARTMENTDETAILS (DEPARTMENT_NAME,DEPARTMENT_SHORT_NAME) VALUES "
                    + "('" + departmentName + "','" + shortName + "')";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";
            } else {
                return "Saved sucessfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(DepartmentDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Save failed";

        }
    }

    @Override
    public DepartmentDetails readDepartmentDetail(Integer Id) {
        {
             DepartmentDetails departmentDetails =null;
            try {
                Connection connection = getConnection();
                Statement statement = connection.createStatement();
            String query = "SELECT * FROM DEPARTMENTDETAILS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                Integer id =resultSet.getInt("ID");
                String departmentName = resultSet.getString("DEPARTMENT_NAME");
                String shortName =resultSet.getString("DEPARTMENT_SHORT_NAME");
                departmentDetails = new DepartmentDetails(id,departmentName,shortName);
            }
            } catch (SQLException ex) {
             
                Logger.getLogger(DepartmentDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                
            }
            return departmentDetails;
        }
        

    }
    @Override
    public List<DepartmentDetails> getAllDepartmentDetails(){
        List<DepartmentDetails> Departments = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM DEPARTMENTDETAILS";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             int id = resultSet.getInt("ID");
                String departmentName = resultSet.getString("DEPARTMENT_NAME");
                String shortName = resultSet.getString("DEPARTMENT_SHORT_NAME");
                DepartmentDetails departmentDetails = new DepartmentDetails(id,departmentName,shortName);
                Departments.add(departmentDetails);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(DepartmentDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return Departments;
    
    }
    
    @Override
    public String updateDepartmentDetails(Integer id,String departmentName,String shortName){
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE DEPARTMENTDETAILS SET DEPARTMENT_NAME ='"+departmentName+"',DEPARTMENT_SHORT_NAME='"+shortName+"' WHERE ID="+id;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(Exception ex){
            return "Update failed";
        }
    }
    
    @Override
    public String deleteDepartmentdetails(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM DEPARTMENTDETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (Exception ex) {
           ex.printStackTrace();
           return "Delete failed";
        }
       
       
    }


    

    
    
                
}