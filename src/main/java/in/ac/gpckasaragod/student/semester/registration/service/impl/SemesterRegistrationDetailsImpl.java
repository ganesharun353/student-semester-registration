/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.semester.registration.service.impl;

import in.ac.gpckasaragod.student.semester.registration.model.ui.data.SemesterRegistrationDetails;
import in.ac.gpckasaragod.student.semester.registration.service.SemesterRegistrationDetailsService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class SemesterRegistrationDetailsImpl extends ConnectionServiceImpl implements SemesterRegistrationDetailsService {

   
    
    @Override
    public String saveSemesterRegistrationDetails(String candidateName, String registerNo, String gender, String departmentId, String currentSem, String revision) {
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = " INSERT INTO SEMESTERREGISTRATION (CANDIDATE_NAME,REGISTER_NUMBER,GENDER,DEPARTMENT_ID,CURRENT_SEMESTER,REVISION) VALUES "
                    + "('" + candidateName + "'," + registerNo + ",'" + gender + "'," + departmentId + ",'" +currentSem +"'," + revision +")";
            System.err.println("Query:" + query);
            int status = statement.executeUpdate(query);
            if (status != 1) {
                return "Save failed";
            } else {
                return "Saved sucessfully";
            }
        } catch (SQLException ex) {
            Logger.getLogger(DepartmentDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            return "Save failed";

        }
    
    
}

    @Override
    public SemesterRegistrationDetails readSemesterRegistrationDetails(Integer Id) {
        {
             SemesterRegistrationDetails semesterRegistrationDetails =null;
            try {
                Connection connection = getConnection();
                Statement statement = connection.createStatement();
            String query = "SELECT * FROM SEMESTERREGISTRATION WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                Integer id =resultSet.getInt("ID");
                String candidateName =resultSet.getString("CANDIDATE_NAME");
                String registerNo =resultSet.getString("REGISTER_NUMBER");
                String gender =resultSet.getString("GENDER");
                Integer departmentId =resultSet.getInt("DEPARTMENT_ID");
                String currentSem =resultSet.getString("CURRENT_SEMESTER");
                Integer revision =resultSet.getInt("REVISION");
                semesterRegistrationDetails = new  SemesterRegistrationDetails(id,candidateName,registerNo,gender,departmentId,currentSem, revision);
            }
            } catch (SQLException ex) {
             
                Logger.getLogger(SemesterRegistrationDetailsImpl.class.getName()).log(Level.SEVERE, null, ex);
                
            }
            return semesterRegistrationDetails;
        }
        

    }
    @Override
    public List<SemesterRegistrationDetails> getAllSemesterRegistrationDetails(){
        List<SemesterRegistrationDetails> semesters = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM SEMESTERREGISTRATION";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             Integer id =resultSet.getInt("ID");
                String candidateName =resultSet.getString("CANDIDATE_NAME");
                String registerNo =resultSet.getString("REGISTER_NUMBER");
                String gender =resultSet.getString("GENDER");
                Integer departmentId =resultSet.getInt("DEPARTMENT_ID");
                String currentSem =resultSet.getString("CURRENT_SEMESTER");
                Integer revision =resultSet.getInt("REVISION");
                SemesterRegistrationDetails semesterRegistrationDetails = new SemesterRegistrationDetails(id,candidateName,registerNo,gender,departmentId,currentSem,revision);
                semesters.add(semesterRegistrationDetails);
            }
        
    }   catch (SQLException ex) {
            Logger.getLogger(DepartmentDetailsServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return semesters;
    
    }
    
    
    @Override
     public String updateSemesterRegistrationDetails(Integer selectedId, String candidateName, String registerNo, String gender, String departmentId, String currentSem, String revision){
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE SEMESTERREGISTRATION SET CANDIDATE_NAME ='"+candidateName+"',REGISTER_NUMBER='"+registerNo+"',GENDER='"+gender+"',DEPARTMENT_ID='"+departmentId+"',CURRENT_SEMESTER='"+currentSem+"',REVISION='"+revision+"' WHERE ID="+selectedId;
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(Exception ex){
            return "Update failed";
        }
    }
    
    @Override
    public String deleteSemesterRegistrationDetails(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM SEMESTERREGISTRATION WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (Exception ex) {
           ex.printStackTrace();
           return "Delete failed";
        }
       
       
    }

    
}